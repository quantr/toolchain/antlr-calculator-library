package hk.quantr.antlrcalculatorlibrary;

import hk.quantr.antlrcalculatorlibrary.antlr.CalculatorLexer;
import hk.quantr.antlrcalculatorlibrary.antlr.CalculatorParser;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTree;
import org.junit.Test;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class Test1 {

	@Test
	public void test() throws Exception {
		ErrorListener errorListener = new ErrorListener();
//		CalculatorLexer lexer = new CalculatorLexer(CharStreams.fromString("(0x1+0x2)*0x3*(0x1-3)"));
		CalculatorLexer lexer = new CalculatorLexer(CharStreams.fromString("0b10110100"));
		lexer.removeErrorListeners();
		lexer.addErrorListener(errorListener);
		CommonTokenStream tokenStream = new CommonTokenStream(lexer);

		CalculatorParser parser = new CalculatorParser(tokenStream);

		parser.removeErrorListeners();
		parser.addErrorListener(errorListener);

		ParseTree tree = parser.input();

		CalculatorBaseVisitorImpl calcVisitor = new CalculatorBaseVisitorImpl();
		long result = calcVisitor.visit(tree);
		System.out.println("Result: " + result);
	}
}
