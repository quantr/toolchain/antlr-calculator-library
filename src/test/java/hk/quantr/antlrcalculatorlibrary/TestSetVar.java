package hk.quantr.antlrcalculatorlibrary;

import hk.quantr.antlrcalculatorlibrary.antlr.CalculatorLexer;
import hk.quantr.antlrcalculatorlibrary.antlr.CalculatorParser;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTree;
import org.apache.commons.io.IOUtils;
import org.junit.Test;

/**
 *
 *
 * @author Peter <peter@quantr.hk>
 */
public class TestSetVar {

	@Test
	public void test() throws Exception {
		ErrorListener errorListener = new ErrorListener();
		String str = IOUtils.toString(getClass().getResourceAsStream("/test1.txt"), "utf8");
		CalculatorLexer lexer = new CalculatorLexer(CharStreams.fromString(str));
		lexer.removeErrorListeners();
		lexer.addErrorListener(errorListener);
		CommonTokenStream tokenStream = new CommonTokenStream(lexer);

		CalculatorParser parser = new CalculatorParser(tokenStream);

		parser.removeErrorListeners();
		parser.addErrorListener(errorListener);

		ParseTree tree = parser.input();

		CalculatorBaseVisitorImpl calcVisitor = new CalculatorBaseVisitorImpl();
		long result = calcVisitor.visit(tree);
		System.out.println("Result: " + result);
	}
}
