package hk.quantr.antlrcalculatorlibrary;

import hk.quantr.antlrcalculatorlibrary.antlr.CalculatorLexer;
import hk.quantr.antlrcalculatorlibrary.antlr.CalculatorParser;
import java.util.ArrayList;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTree;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class CalculatorLibrary {

	public static String cal(String mathExpression, ArrayList<Label> labels) {
		CalculatorLexer lexer = new CalculatorLexer(CharStreams.fromString(mathExpression));
		CommonTokenStream tokenStream = new CommonTokenStream(lexer);

		CalculatorParser parser = new CalculatorParser(tokenStream);
		ParseTree tree = parser.input();

		CalculatorBaseVisitorImpl calcVisitor = new CalculatorBaseVisitorImpl();
		long result = calcVisitor.visit(tree);
		return String.valueOf(result);
	}

	public static String cal(String mathExpression) {
		CalculatorLexer lexer = new CalculatorLexer(CharStreams.fromString(mathExpression));
		CommonTokenStream tokenStream = new CommonTokenStream(lexer);

		CalculatorParser parser = new CalculatorParser(tokenStream);
		ParseTree tree = parser.input();

		CalculatorBaseVisitorImpl calcVisitor = new CalculatorBaseVisitorImpl();
		long result = calcVisitor.visit(tree);
		return String.valueOf(result);
	}

	public static long calLong(String mathExpression) {
		CalculatorLexer lexer = new CalculatorLexer(CharStreams.fromString(mathExpression));
		CommonTokenStream tokenStream = new CommonTokenStream(lexer);

		CalculatorParser parser = new CalculatorParser(tokenStream);
		ParseTree tree = parser.input();

		CalculatorBaseVisitorImpl calcVisitor = new CalculatorBaseVisitorImpl();
		long result = calcVisitor.visit(tree);
		return result;
	}

	public static long calLong(String mathExpression, ArrayList<Label> labels) {
		CalculatorLexer lexer = new CalculatorLexer(CharStreams.fromString(mathExpression));
		CommonTokenStream tokenStream = new CommonTokenStream(lexer);

		CalculatorParser parser = new CalculatorParser(tokenStream);
		ParseTree tree = parser.input();

		CalculatorBaseVisitorImpl calcVisitor = new CalculatorBaseVisitorImpl();
		long result = calcVisitor.visit(tree);
		return result;
	}

	public static double calDouble(String mathExpression, ArrayList<Label> labels) {
		CalculatorLexer lexer = new CalculatorLexer(CharStreams.fromString(mathExpression));
		CommonTokenStream tokenStream = new CommonTokenStream(lexer);

		CalculatorParser parser = new CalculatorParser(tokenStream);
		ParseTree tree = parser.input();

		CalculatorBaseVisitorImpl calcVisitor = new CalculatorBaseVisitorImpl();
		long result = calcVisitor.visit(tree);
		return result;
	}
}
