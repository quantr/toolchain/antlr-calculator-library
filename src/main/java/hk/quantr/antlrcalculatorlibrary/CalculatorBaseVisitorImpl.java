package hk.quantr.antlrcalculatorlibrary;

import hk.quantr.antlrcalculatorlibrary.antlr.CalculatorBaseVisitor;
import hk.quantr.antlrcalculatorlibrary.antlr.CalculatorParser;
import hk.quantr.javalib.CommonLib;
import java.util.HashMap;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class CalculatorBaseVisitorImpl extends CalculatorBaseVisitor<Long> {

	private HashMap<String, Long> variables = new HashMap<String, Long>();

//	@Override
//	public Long visitCalculatorInput(CalculatorParser.CalculatorInputContext ctx) {
//		return visitChildren(ctx.input2());
//	}
	@Override
	public Long visitPlus(CalculatorParser.PlusContext ctx) {
		return visit(ctx.plusOrMinus()) + visit(ctx.multOrDiv());
	}

	@Override
	public Long visitMinus(CalculatorParser.MinusContext ctx) {
		return visit(ctx.plusOrMinus()) - visit(ctx.multOrDiv());
	}

	@Override
	public Long visitMultiplication(CalculatorParser.MultiplicationContext ctx) {
		return visit(ctx.multOrDiv()) * visit(ctx.pow());
	}

	@Override
	public Long visitDivision(CalculatorParser.DivisionContext ctx) {
		return visit(ctx.multOrDiv()) / visit(ctx.pow());
	}

	@Override
	public Long visitToSetVar(CalculatorParser.ToSetVarContext ctx) {
		visit(ctx.setVar());
		return visit(ctx.input());
	}

	@Override
	public Long visitSetVariable(CalculatorParser.SetVariableContext ctx) {
		Long value = visit(ctx.plusOrMinus());
		variables.put(ctx.ID().getText(), value);
		System.out.println("set " + ctx.ID().getText() + " = " + value);
		return value;
	}

	@Override
	public Long visitPower(CalculatorParser.PowerContext ctx) {
		if (ctx.pow() != null) {
			return (long) Math.pow(visit(ctx.unaryMinus()), visit(ctx.pow()));
		}
		return visit(ctx.unaryMinus());
	}

	@Override
	public Long visitChangeSign(CalculatorParser.ChangeSignContext ctx) {
		return -1 * visit(ctx.unaryMinus());
	}

	@Override
	public Long visitBraces(CalculatorParser.BracesContext ctx) {
		return visit(ctx.plusOrMinus());
	}

	@Override
	public Long visitConstantPI(CalculatorParser.ConstantPIContext ctx) {
		return (long) Math.PI;
	}

	@Override
	public Long visitConstantE(CalculatorParser.ConstantEContext ctx) {
		return (long) Math.E;
	}

	@Override
	public Long visitInt(CalculatorParser.IntContext ctx) {
		return Long.parseLong(ctx.INT().getText());
	}

	@Override
	public Long visitHex(CalculatorParser.HexContext ctx) {
		return CommonLib.string2long(ctx.getText());
	}

	@Override
	public Long visitBinary(CalculatorParser.BinaryContext ctx) {
		return Long.parseLong(ctx.getText().replaceFirst("0b", ""), 2);
	}

	@Override
	public Long visitVariable(CalculatorParser.VariableContext ctx) {
		System.out.println("len = " + variables.size());
		for (String key : variables.keySet()) {
			System.out.println("\t\t>" + key + "< = " + variables.get(key));
		}
		System.out.println("get var >" + ctx.ID().getText() + "< = " + variables.get(ctx.ID().getText()));
		return variables.get(ctx.ID().getText());
	}

	@Override
	public Long visitDouble(CalculatorParser.DoubleContext ctx) {
		return Long.parseLong(ctx.DOUBLE().getText());
	}

	@Override
	public Long visitCalculate(CalculatorParser.CalculateContext ctx) {
		return visit(ctx.plusOrMinus());
	}

}
