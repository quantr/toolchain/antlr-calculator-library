/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hk.quantr.antlrcalculatorlibrary;

import java.util.BitSet;
import org.antlr.v4.runtime.BaseErrorListener;
import org.antlr.v4.runtime.Parser;
import org.antlr.v4.runtime.RecognitionException;
import org.antlr.v4.runtime.Recognizer;
import org.antlr.v4.runtime.Token;
import org.antlr.v4.runtime.atn.ATNConfigSet;
import org.antlr.v4.runtime.dfa.DFA;

/**
 *
 * @author Peter
 */
public class ErrorListener extends BaseErrorListener {

	@Override
	public void syntaxError(final Recognizer<?, ?> recognizer, final Object offendingSymbol, final int line, final int position, final String msg, final RecognitionException e) {
		Token offendingToken = (Token) offendingSymbol;
		if (offendingToken == null) {
			return;
		}
		int start = offendingToken.getStartIndex();
		int stop = offendingToken.getStopIndex();
		if (start > stop) {
			int temp = start;
			start = stop;
			stop = temp;
		}
		System.out.println("ERROR " + line + ":" + position + ", " + offendingToken.getStartIndex() + ", " + offendingToken.getStopIndex() + ": " + msg);
	}

	public void reportAmbiguity(Parser recognizer, DFA dfa, int startIndex, int stopIndex, boolean exact, BitSet ambigAlts, ATNConfigSet configs) {
		System.out.println("reportAmbiguity");
	}

	public void reportAttemptingFullContext(Parser recognizer, DFA dfa, int startIndex, int stopIndex, BitSet conflictingAlts, ATNConfigSet configs) {
		System.out.println("reportAttemptingFullContext");
	}

	public void reportContextSensitivity(Parser recognizer, DFA dfa, int startIndex, int stopIndex, int prediction, ATNConfigSet configs) {
		System.out.println("reportContextSensitivity");
	}
}
