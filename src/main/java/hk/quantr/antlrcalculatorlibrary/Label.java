package hk.quantr.antlrcalculatorlibrary;

/**
 *
 * @author Peter
 */
public class Label {

	String name;
	int lineNo;
	int offset;

	public Label(String name, int lineNo, int offset) {
		this.name = name;
		this.lineNo = lineNo;
		this.offset = offset;
	}

	@Override
	public String toString() {
		return "Label{name=" + name + ", lineNo=" + lineNo + ", offset=" + offset + '}';
	}
}
